/* Schemas based in interface for typescript */

export interface ApplicantResponse {
  jobId: any;
  metadata?: {
    [k: string]: any;
  };
  applicant: {
    name: string;
    lastnames: string;
    country: "peru" | "chile" | "ecuador" | "mexico" | "colombia";
    description?: string;
    curriculum: string;
    phone?: string;
  };
  createdAt: number;
}

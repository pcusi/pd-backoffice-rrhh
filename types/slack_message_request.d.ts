/* Schemas based in interface for typescript */

export interface SlackMessageRequest {
  channel: string;
  text: string;
}

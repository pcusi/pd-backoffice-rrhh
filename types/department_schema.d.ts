/* Schemas based in interface for typescript */

export interface DepartmentResponse {
  id?: any;
  name: string;
  leader: any;
  areaId: any;
  createdAt?: number;
  updatedAt?: number;
}

/* Schemas based in interface for typescript */

export interface GroupResponse {
  id?: any;
  name: string;
  leader?: string;
  members: any[];
  departmentId: any;
  createdAt: number;
  updatedAt: number;
}

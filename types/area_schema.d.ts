/* Schemas based in interface for typescript */

export interface AreaResponse {
  id?: any;
  name: string;
  createdAt: number;
  updatedAt: number;
}

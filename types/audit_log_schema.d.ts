/* Schemas based in interface for typescript */

export interface AuditLogRequest {
  userId: any;
  operation: string;
  createdAt: number;
  operationIp: string;
  operationOs: string;
  operationCollection: string;
  operationStatus: string;
  operationStatusCode: number;
}

/* Schemas based in interface for typescript */

export interface JobResponse {
  id?: any;
  name: string;
  vacancies: number;
  tags: string[];
  description: string;
  createdAt: number;
  updatedAt: number;
  location?: string;
  endAt?: number;
  jobType?: string;
  userId?: any;
}

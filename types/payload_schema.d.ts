/* Schemas based in interface for typescript */

export interface PayloadResponse {
  sub?: string;
  role?: "super-admin" | "admin" | "resource" | "employee";
  user?: UserResponse;
}
export interface UserResponse {
  _id?: any;
  names: string;
  lastnames: string;
  email: string;
  country: "peru" | "chile" | "ecuador" | "mexico" | "colombia";
  password?: string;
  phone?: string;
  documentType?: "dni" | "passport" | "foreigner-card" | "ruc";
  documentNumber: string;
  metadata?: any;
  createdAt?: number;
  updatedAt?: number;
  hasContract?: boolean;
  role?: "super-admin" | "admin" | "resource" | "employee";
}

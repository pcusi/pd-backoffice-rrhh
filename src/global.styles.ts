import { GlobalStylesProps as StyledGlobalStylesProps } from "@mui/styled-engine/GlobalStyles/GlobalStyles";
import { Theme } from "@mui/system";
import { Palette } from "@themes/theme";

export const globalStyles: StyledGlobalStylesProps<Theme>["styles"] = {
  body: {
    backgroundColor: Palette.background.primary,
    padding: 0,
    margin: 0,
    maxWidth: "100%",
  },
};

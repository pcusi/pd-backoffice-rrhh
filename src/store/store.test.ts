import { authInitialState } from "@store/reducers/auth/auth.slice";
import { jobInitialState } from "@store/reducers/recruitment/job/job.slice";
import { userInitialState } from "@store/reducers/user/user.slice";
import { store } from "@store/store";

describe("Global store", () => {
  it("should return the initial global state", () => {
    const initialGlobalState = store.getState();

    expect(initialGlobalState).toEqual({
      auth: authInitialState,
      user: userInitialState,
      job: jobInitialState,
    });
  });
});

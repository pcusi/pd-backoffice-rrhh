import { configureStore } from "@reduxjs/toolkit";
import authSlice from "@store/reducers/auth/auth.slice";
import jobSlice from "@store/reducers/recruitment/job/job.slice";
import userSlice from "@store/reducers/user/user.slice";

export const store = configureStore({
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  reducer: {
    auth: authSlice,
    user: userSlice,
    job: jobSlice,
  },
});

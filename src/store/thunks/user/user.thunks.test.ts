import { mock_users, user_form_values } from "@core/infrastructure";
import { AnyAction } from "@reduxjs/toolkit";
import axios from "@shared/utils/axios-auth";
import { switchModal, updateUserForm } from "@store/actions/user.actions";
import userSlice, { userInitialState } from "@store/reducers/user/user.slice";
import { store } from "@store/store";
import { createUser, getUsers, updateUser } from "@store/thunks/user/user.thunks";
import { UserResponse } from "types/user_schema";

jest.mock("axios", () => {
  return {
    create: jest.fn(() => ({
      get: jest.fn(),
      post: jest.fn(),
      patch: jest.fn(),
      delete: jest.fn(),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() },
      },
    })),
  };
});

describe("User Thunk Testing", () => {
  it("When getUser thunk is called successfully, then should call axios.post and set loading to false", async () => {
    (axios.get as jest.Mock).mockResolvedValue({
      data: {
        users: [],
      },
    });

    await store.dispatch(getUsers());

    expect(axios.get).toBeCalled();
    expect(store.getState().user.users).toHaveLength(0);
  });

  it("When auth.reject is send, then should set error to true, and auth to failed", () => {
    const action: AnyAction = {
      type: getUsers.rejected.type,
    };

    expect(userSlice(userInitialState, action)).toEqual({
      ...userInitialState,
      loading: false,
    });
  });

  it("When createUser thunk is called successfully, then should call axios.post and set loading to false", async () => {
    (axios.post as jest.Mock).mockResolvedValue({
      data: {
        message: "Created!",
      },
    });

    await store.dispatch(createUser({ ...mock_users }));

    expect(axios.post).toBeCalled();
    expect(store.getState().user.loading).toBe(false);
  });

  it("When createUser.reject is send, then should set loading to false", async () => {
    const action: AnyAction = {
      type: createUser.rejected.type,
    };

    expect(userSlice(userInitialState, action)).toEqual({
      ...userInitialState,
      loading: false,
    });
  });

  it("When updateUser thunk is called successfully, then should call axios.put and set loading to false", async () => {
    (axios.patch as jest.Mock).mockResolvedValue({
      data: {
        message: "Created!",
      },
    });

    await store.dispatch(updateUser({ ...mock_users }));

    expect(axios.patch).toBeCalled();
    expect(store.getState().user.loading).toBe(false);
    expect(store.getState().user.modal).toBe(false);
    expect(store.getState().user.userForm).toBe(undefined);
  });

  it("When updateUser.reject is send, then should set loading to false", async () => {
    const action: AnyAction = {
      type: updateUser.rejected.type,
    };

    expect(userSlice(userInitialState, action)).toEqual({
      ...userInitialState,
      loading: false,
    });
  });

  it("When updateUserForm is dispatched, userForm property names should be Piero", () => {
    const names: string = "Piero";

    const payload: UserResponse = {
      ...user_form_values,
      names,
    };

    expect(userSlice(userInitialState, updateUserForm(payload))).toEqual({
      ...userInitialState,
      userForm: {
        ...user_form_values,
        names,
      },
    });
  });

  it("When switchModal is dispatched, modal prop should be true", () => {
    const payload: boolean = true;

    expect(userSlice(userInitialState, switchModal(payload))).toEqual({
      ...userInitialState,
      modal: true,
    });
  });
});

import { API_ROUTES } from "@core/infrastructure";
import { ThunkUserPrefixes } from "@infrastructure/enum";
import { createAsyncThunk } from "@reduxjs/toolkit";
import axiosAuth from "@shared/utils/axios-auth";
import { UserResponse } from "types/user_schema";

export const getUsers = createAsyncThunk(ThunkUserPrefixes.USERS, async () => {
  const response = await axiosAuth.get(API_ROUTES.USERS);

  return response.data;
});

export const createUser = createAsyncThunk<any, UserResponse>(
  ThunkUserPrefixes.SAVE,
  async (payload) => {
    const response = await axiosAuth.post(API_ROUTES.SAVE_USER, { ...payload });

    return response.data;
  },
);

export const updateUser = createAsyncThunk<any, UserResponse>(
  ThunkUserPrefixes.UPDATE,
  async (payload) => {
    const response = await axiosAuth.patch(API_ROUTES.EDIT_USER(payload._id), {
      ...payload,
    });

    return response.data;
  },
);

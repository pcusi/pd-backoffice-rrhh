import { API_ROUTES } from "@infrastructure/constants/api.constant";
import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "@shared/utils/axios-auth";
import { UserResponse } from "types/user_schema";

export interface PayloadResponse {
  token: string;
  user: UserResponse;
}

export const auth = createAsyncThunk<PayloadResponse, UserResponse>(
  "/auth/sign-in",
  async (payload) => {
    const response = await axios.post(API_ROUTES.AUTH, payload);

    return response.data;
  },
);

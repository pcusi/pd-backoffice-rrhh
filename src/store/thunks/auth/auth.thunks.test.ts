import { AnyAction } from "@reduxjs/toolkit";
import axios from "@shared/utils/axios-auth";
import { resetForm, updateUserForm } from "@store/actions/auth.actions";
import authSlice, { authInitialState } from "@store/reducers/auth/auth.slice";
import { store } from "@store/store";
import { auth } from "@store/thunks/auth/auth.thunks";
import { UserResponse } from "types/user_schema";

jest.mock("axios", () => {
  return {
    create: jest.fn(() => ({
      get: jest.fn(),
      post: jest.fn(),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() },
      },
    })),
  };
});

describe("Auth Thunk Testing", () => {
  it("When auth thunk is called successfully, then should call axios.post and set loading to false", async () => {
    (axios.post as jest.Mock).mockResolvedValue({
      data: {
        token: "123",
        user: {
          ...authInitialState.user,
        },
      },
    });

    await store.dispatch(
      auth({
        email: "pcusir@gmail.com",
        password: "123",
        country: "peru",
        documentNumber: "",
        lastnames: "",
        names: "",
      }),
    );

    expect(axios.post).toBeCalled();
    expect(store.getState().auth.loading).toBe(false);
  });

  it("When auth.reject is send, then should set error to true, and auth to failed", () => {
    const action: AnyAction = {
      payload: {
        email: "pcusir@gmail.com",
        password: "",
      },
      type: auth.rejected.type,
    };

    expect(authSlice(authInitialState, action)).toEqual({
      ...authInitialState,
      loading: false,
    });
  });

  it("When updateUserForm is dispatched, should be equal to the initialState", () => {
    const payload: UserResponse = {
      ...authInitialState.user,
    };

    expect(authSlice(authInitialState, updateUserForm(payload))).toEqual({
      ...authInitialState,
    });
  });

  it("When resetForm is dispatched, should be equal to the initialState", () => {
    expect(authSlice(authInitialState, resetForm())).toEqual({
      ...authInitialState,
    });
  });
});

import { AnyAction } from "@reduxjs/toolkit";
import axios from "@shared/utils/axios-auth";
import jobSlice, {
  jobInitialState,
} from "@store/reducers/recruitment/job/job.slice";
import { store } from "@store/store";
import { getJobs } from "@store/thunks/recruitment/jobs/jobs.thunks";

jest.mock("axios", () => {
  return {
    create: jest.fn(() => ({
      get: jest.fn(),
      post: jest.fn(),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() },
      },
    })),
  };
});

describe("Job Thunk Testing", () => {
  it("When getJob thunk is called successfully, then should call axios.get and set loading to false", async () => {
    (axios.get as jest.Mock).mockResolvedValue({
      data: {
        jobs: [],
      },
    });

    await store.dispatch(getJobs());

    expect(axios.get).toBeCalled();
    expect(store.getState().job.jobs).toHaveLength(0);
  });

  it("When getJob reject type is called, should return loading to false", async () => {
    const action: AnyAction = {
      type: getJobs.rejected.type,
    };

    expect(jobSlice(jobInitialState, action)).toEqual({
      ...jobInitialState,
      loading: false,
    });
  });
});

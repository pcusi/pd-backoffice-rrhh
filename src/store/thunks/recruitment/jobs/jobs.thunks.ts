import { API_ROUTES } from "@core/infrastructure";
import { ThunkJobPrefixes } from "@infrastructure/enum";
import { createAsyncThunk } from "@reduxjs/toolkit";
import axiosAuth from "@shared/utils/axios-auth";

export const getJobs = createAsyncThunk(ThunkJobPrefixes.JOBS, async () => {
  const response = await axiosAuth.get(API_ROUTES.JOBS);

  return response.data;
});

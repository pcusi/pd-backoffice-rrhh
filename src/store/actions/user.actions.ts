import { userSlice } from "@store/reducers/user/user.slice";

export const { updateUserForm, switchModal } = userSlice.actions;

import { authSlice } from "@store/reducers/auth/auth.slice";

export const { updateUserForm, resetForm } = authSlice.actions;

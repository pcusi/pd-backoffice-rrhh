import { Slices } from "@infrastructure/enum";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { createUser, getUsers, updateUser } from "@store/thunks/user/user.thunks";
import { UserResponse } from "types/user_schema";

export interface UserState {
  loading: boolean;
  users: UserResponse[];
  userForm: UserResponse | undefined;
  modal: boolean;
}

export const userInitialState: UserState = {
  loading: false,
  users: [],
  userForm: undefined,
  modal: false,
};

export const userSlice = createSlice({
  name: Slices.user,
  initialState: userInitialState,
  reducers: {
    updateUserForm: (state: UserState, { payload }: PayloadAction<UserResponse>) => {
      state.userForm = {
        ...payload,
      };
    },
    switchModal: (state: UserState, { payload }: PayloadAction<boolean>) => {
      state.modal = payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(updateUser.pending, (state: UserState) => {
        state.loading = true;
      })
      .addCase(updateUser.rejected, (state: UserState) => {
        state.loading = false;
      })
      .addCase(updateUser.fulfilled, (state: UserState) => {
        state.loading = false;
        state.userForm = undefined;
        state.modal = false;
      })
      .addCase(createUser.pending, (state: UserState) => {
        state.loading = true;
      })
      .addCase(createUser.rejected, (state: UserState) => {
        state.loading = false;
      })
      .addCase(createUser.fulfilled, (state: UserState) => {
        state.loading = false;
      })
      .addCase(getUsers.pending, (state: UserState) => {
        state.loading = true;
      })
      .addCase(getUsers.rejected, (state: UserState) => {
        state.loading = false;
      })
      .addCase(getUsers.fulfilled, (state: UserState, { payload }) => {
        state.loading = false;

        state.users = payload.users;
      });
  },
});

export default userSlice.reducer;

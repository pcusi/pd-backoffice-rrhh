import { AnyAction } from "@reduxjs/toolkit";
import userSlice, { userInitialState } from "@store/reducers/user/user.slice";

describe("User Slice Testing", () => {
  it("When app is render, it must return initial state", () => {
    expect(userSlice(undefined, {} as AnyAction)).toEqual(userInitialState);
  });
});

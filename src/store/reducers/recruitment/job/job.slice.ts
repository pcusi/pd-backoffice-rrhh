import { Slices } from "@infrastructure/enum";
import { createSlice } from "@reduxjs/toolkit";
import { getJobs } from "@store/thunks/recruitment/jobs/jobs.thunks";
import { JobResponse } from "types/job_schema";

export interface JobState {
  loading: boolean;
  jobs: JobResponse[];
}

export const jobInitialState: JobState = {
  loading: false,
  jobs: [],
};

export const jobSlice = createSlice({
  name: Slices.job,
  initialState: jobInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getJobs.pending, (state: JobState) => {
        state.loading = true;
      })
      .addCase(getJobs.rejected, (state: JobState) => {
        state.loading = false;
      })
      .addCase(getJobs.fulfilled, (state: JobState, { payload }) => {
        state.loading = false;

        state.jobs = payload.jobs;
      });
  },
});

export default jobSlice.reducer;

import { AnyAction } from "@reduxjs/toolkit";
import jobSlice, {
  jobInitialState,
} from "@store/reducers/recruitment/job/job.slice";

describe("Job Slice Testing", () => {
  it("When app is render, it must return initial state", () => {
    expect(jobSlice(undefined, {} as AnyAction)).toEqual(jobInitialState);
  });
});

import { CustomHeaders, Slices } from "@infrastructure/enum";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { auth } from "@store/thunks/auth/auth.thunks";
import { UserResponse } from "types/user_schema";

export interface AuthState {
  loading: boolean;
  user: UserResponse;
}

export const authInitialState: AuthState = {
  loading: false,
  user: {
    email: "",
    password: "",
    names: "",
    lastnames: "",
    country: "peru",
    documentType: "dni",
    documentNumber: "",
  },
};

export const authSlice = createSlice({
  name: Slices.auth,
  initialState: authInitialState,
  reducers: {
    updateUserForm: (state: AuthState, { payload }: PayloadAction<UserResponse>) => {
      state.user = payload;
    },
    resetForm: (state: AuthState) => {
      state.user = authInitialState.user;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(auth.pending, (state: AuthState) => {
        state.loading = true;
      })
      .addCase(auth.rejected, (state: AuthState) => {
        state.loading = false;
      })
      .addCase(auth.fulfilled, (state: AuthState, { payload }) => {
        state.loading = false;
        localStorage.setItem(CustomHeaders.TOKEN, payload.token);
        localStorage.setItem(CustomHeaders.USER, JSON.stringify(payload.user));
        localStorage.setItem(CustomHeaders.NAME, payload.user.names);

        window.location.href = "/admin/dashboard";
      });
  },
});

export default authSlice.reducer;

import { AnyAction } from "@reduxjs/toolkit";
import authSlice, { authInitialState } from "@store/reducers/auth/auth.slice";

describe("Auth Slice Testing", () => {
  it("When app is render, it must return initial state", () => {
    expect(authSlice(undefined, {} as AnyAction)).toEqual(authInitialState);
  });
});

import { Dashboard, Jobs, Users } from "@components/admin";
import { AuthSignIn } from "@components/auth";
import { Admin } from "@core/layouts";
import { Path } from "@infrastructure/enum";
import React from "react";
import { Navigate, createBrowserRouter } from "react-router-dom";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Navigate to="/auth" replace />,
  },
  {
    path: "/auth",
    children: [
      {
        path: "/auth",
        element: <AuthSignIn />,
      },
    ],
  },
  {
    path: "/admin",
    element: <Admin />,
    children: [
      {
        path: Path.DASHBOARD,
        element: <Dashboard />,
      },
      {
        path: Path.USERS,
        element: <Users />,
      },
      {
        path: Path.JOBS,
        element: <Jobs />,
      },
    ],
  },
]);

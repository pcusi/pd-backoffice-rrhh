import { Countries, Roles } from "@infrastructure/enum";
import {
  Control,
  DeepPartial,
  EventType,
  FieldValues,
  InternalFieldName,
} from "react-hook-form";
import { DefaultValues } from "react-hook-form/dist/types/form";
import { Subject } from "rxjs";
import { JobResponse } from "types/job_schema";
import { NotificationResponse } from "types/notification_schema";
import { UserResponse } from "types/user_schema";

function createMockSubject(): Subject<{
  name?: InternalFieldName;
  type?: EventType;
  values: FieldValues;
}> {
  return new Subject<{
    name?: InternalFieldName;
    type?: EventType;
    values: FieldValues;
  }>();
}

function createMockArraySubject(): Subject<{
  name?: InternalFieldName;
  values?: FieldValues;
}> {
  return new Subject<{
    name?: InternalFieldName;
    values?: FieldValues;
  }>();
}

function createMockValuesSubject(): Subject<{
  name?: InternalFieldName;
}> {
  return new Subject<{
    name?: InternalFieldName;
  }>();
}

type AsyncDefaultValues<TFieldValues> = (payload?: unknown) => Promise<TFieldValues>;

export const mockControlHookForm = <T extends FieldValues>(
  defaultValues: Partial<DeepPartial<T>>,
  optionDefaultValues: DefaultValues<T> | AsyncDefaultValues<T>,
  readonlyDefaultValues: Readonly<DeepPartial<T>>,
): Control<T, object> => {
  return {
    register: jest.fn(),
    unregister: jest.fn(),
    getFieldState: jest.fn(),
    _removeUnmounted: jest.fn(),
    _state: {
      mount: false,
      action: false,
      watch: true,
    },
    _options: {
      mode: "onChange",
      reValidateMode: "onChange",
      defaultValues: optionDefaultValues,
      shouldFocusError: false,
      shouldUnregister: false,
      shouldUseNativeValidation: false,
      delayError: 0,
    },
    _formState: {
      isDirty: false,
      isLoading: false,
      isSubmitted: false,
      isSubmitSuccessful: false,
      isSubmitting: false,
      isValidating: false,
      isValid: false,
      submitCount: 0,
      defaultValues: readonlyDefaultValues,
      dirtyFields: {},
      touchedFields: {},
      errors: {},
    },
    _fields: {},
    _getFieldArray: jest.fn(),
    _executeSchema: jest.fn(),
    _getDirty: jest.fn(),
    _updateValid: jest.fn(),
    _updateFormState: jest.fn(),
    _updateFieldArray: jest.fn(),
    _proxyFormState: {
      isDirty: false,
      isValidating: false,
      dirtyFields: false,
      touchedFields: false,
      errors: false,
      isValid: false,
    },
    _reset: jest.fn(),
    _names: {
      array: new Set("test"),
      mount: new Set("test"),
      unMount: new Set("test"),
      watch: new Set("test"),
      focus: "test",
      watchAll: false,
    },
    _subjects: {
      values: createMockSubject(),
      array: createMockArraySubject(),
      state: createMockValuesSubject(),
    },
    _getWatch: jest.fn(),
    _formValues: ["test"],
    _defaultValues: defaultValues,
  };
};

export const mock_users: UserResponse = {
  country: Countries.peru,
  documentNumber: "",
  lastnames: "",
  names: "",
  email: "pcusir@gmail.com",
  role: Roles.ADMIN,
};

export const mock_jobs: JobResponse = {
  name: "",
  vacancies: 0,
  tags: [],
  description: "",
  createdAt: 0,
  updatedAt: 0,
};

export const mock_notifications: NotificationResponse[] = [
  {
    userId: "123",
    name: "Test",
    createdAt: 1,
    read: false,
  },
];

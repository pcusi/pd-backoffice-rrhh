import { environment } from "@environment/environment";

export const API_ROUTES = {
  AUTH: `${environment.ciUrl}/user/sign-in`,
  USERS: `${environment.ciUrl}/users`,
  SAVE_USER: `${environment.ciUrl}/user`,
  EDIT_USER: (userId: string) => `${environment.ciUrl}/user/${userId}`,
  JOBS: `${environment.ciUrl}/recruitment/jobs`,
};

import { Path, Roles } from "@infrastructure/enum";
import { IconType } from "react-Icons";
import { MdFolder, MdHouse, MdPeople } from "react-icons/md";

export interface NavigationRole {
  name: string;
  Icon: IconType;
  roles: Roles[];
  to: string;
}

export const navigation_roles: NavigationRole[] = [
  {
    Icon: MdHouse,
    name: "Dashboard",
    roles: [Roles.SUPER, Roles.RESOURCE, Roles.ADMIN],
    to: Path.DASHBOARD,
  },
  {
    Icon: MdPeople,
    name: "Users",
    roles: [Roles.SUPER, Roles.ADMIN],
    to: Path.USERS,
  },
  {
    Icon: MdFolder,
    name: "Jobs",
    roles: [Roles.RESOURCE],
    to: Path.JOBS,
  },
];

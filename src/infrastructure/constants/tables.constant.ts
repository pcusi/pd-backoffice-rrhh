import {
  ITableHeaders,
  TableCellType,
  TableHeaders,
  TableJobHeaders,
} from "@core/infrastructure";
import {
  get_job_headers_catalog,
  get_user_headers_catalog,
} from "@infrastructure/catalog/custom-headers.catalog";

export interface MapJsonKeyHeaders {
  keys: JsonKey[];
}

export interface JsonKey {
  key: string;
}

export const table_job_headers: ITableHeaders[] = [
  {
    name: get_job_headers_catalog[TableJobHeaders.NAME],
    type: TableCellType.NORMAL,
  },
  {
    name: get_job_headers_catalog[TableJobHeaders.VACANCIES],
    type: TableCellType.CHIP,
  },
  {
    name: get_job_headers_catalog[TableJobHeaders.ID],
    type: TableCellType.OPTION,
  },
];

export const table_user_headers: ITableHeaders[] = [
  {
    name: get_user_headers_catalog[TableHeaders.EMAIL],
    type: TableCellType.NORMAL,
  },
  {
    name: get_user_headers_catalog[TableHeaders.NAMES],
    type: TableCellType.THUMBNAIL,
  },
  {
    name: get_user_headers_catalog[TableHeaders.CREATED_AT],
    type: TableCellType.NORMAL,
  },
  {
    name: get_user_headers_catalog[TableHeaders.ROLE],
    type: TableCellType.CHIP,
  },
  {
    name: get_user_headers_catalog[TableHeaders.ID],
    type: TableCellType.OPTION,
  },
];

export const map_job_headers: MapJsonKeyHeaders[] = [
  {
    keys: [{ key: TableJobHeaders.NAME }],
  },
  {
    keys: [{ key: TableJobHeaders.VACANCIES }],
  },
  {
    keys: [{ key: TableJobHeaders.ID }],
  },
];

export const map_user_headers: MapJsonKeyHeaders[] = [
  {
    keys: [{ key: TableHeaders.EMAIL }],
  },
  {
    keys: [{ key: TableHeaders.NAMES }, { key: TableHeaders.LASTNAMES }],
  },
  {
    keys: [{ key: TableHeaders.CREATED_AT }],
  },
  {
    keys: [{ key: TableHeaders.ROLE }],
  },
  {
    keys: [{ key: TableHeaders.ID }],
  },
];

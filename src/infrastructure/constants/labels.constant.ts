type ModuleWithAction = "title" | "add" | "dialogTitle" | "dialogSubmitTitle";

type AuthLabels = "email" | "password" | "submit";
type AuthPlaceholders = "email" | "password";
type AdminLabels = "logout";
type DashboardLabels = "title";
type UserLabels = ModuleWithAction;
type JobLabels = ModuleWithAction;

export const authLabels: Record<AuthLabels, string> = {
  email: "Email",
  password: "Password",
  submit: "Sign In",
};

export const authPlaceholder: Record<AuthPlaceholders, string> = {
  email: "testing@gmail.com",
  password: "Must be a secret!",
};

export const adminLabels: Record<AdminLabels, string> = {
  logout: "Logout",
};

export const dashboardLabels: Record<DashboardLabels, string> = {
  title: "Dashboard",
};

export const userLabels: Record<UserLabels, string> = {
  title: "Users",
  add: "Add user",
  dialogTitle: "Add user",
  dialogSubmitTitle: "Save",
};

export const jobLabels: Record<JobLabels, string> = {
  title: "Jobs",
  add: "Add job",
  dialogTitle: "Add job",
  dialogSubmitTitle: "Save",
};

import { Countries, DocumentTypes, Roles } from "@infrastructure/enum";
import { UserResponse } from "types/user_schema";

export const user_form_values: UserResponse = {
  email: "",
  names: "",
  lastnames: "",
  documentNumber: "",
  documentType: DocumentTypes.DNI,
  country: Countries.peru,
  role: Roles.RESOURCE,
};

import { Roles } from "@infrastructure/enum";

export const rolesCatalog: Record<string, string> = {
  [Roles.ADMIN]: "Administrator",
  [Roles.SUPER]: "Super Administrator",
  [Roles.RESOURCE]: "Human Resource",
  [Roles.EMPLOYEE]: "Employee",
};

export const roles: string[] = [
  Roles.ADMIN,
  Roles.SUPER,
  Roles.RESOURCE,
  Roles.EMPLOYEE,
];

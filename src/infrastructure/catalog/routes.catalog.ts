import { Path } from "@infrastructure/enum";

type RoutesCatalog = "dashboard" | "users" | "jobs";

export const routes_catalog: Record<RoutesCatalog, string> = {
  [Path.DASHBOARD]: "Dashboard",
  [Path.USERS]: "Users",
  [Path.JOBS]: "Jobs",
};

import { DocumentTypes } from "@infrastructure/enum";

export const document_type_catalog: Record<string, string> = {
  [DocumentTypes.DNI]: "DNI",
  [DocumentTypes.FOREIGNER_CARD]: "Foreigner Card",
  [DocumentTypes.PASSPORT]: "Passport",
  [DocumentTypes.RUC]: "RUC",
};

export const document_types: string[] = [
  DocumentTypes.DNI,
  DocumentTypes.FOREIGNER_CARD,
  DocumentTypes.RUC,
  DocumentTypes.PASSPORT,
];

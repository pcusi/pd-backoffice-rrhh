import { TableHeaders, TableJobHeaders } from "@infrastructure/enum/TableHeaders";

export let get_user_headers_catalog: Record<string, string> = {
  [TableHeaders.EMAIL]: "Email",
  [TableHeaders.NAMES]: "Names",
  [TableHeaders.CREATED_AT]: "Joining Date",
  [TableHeaders.ID]: "Action",
  [TableHeaders.ROLE]: "Role",
};

export let get_job_headers_catalog: Record<string, string> = {
  [TableJobHeaders.NAME]: "Name",
  [TableJobHeaders.VACANCIES]: "Vacancies",
  [TableHeaders.ID]: "Role",
};

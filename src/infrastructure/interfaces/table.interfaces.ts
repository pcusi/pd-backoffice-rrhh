import { MapJsonKeyHeaders } from "@core/infrastructure";

export interface ICustomTable {
  headers: ITableHeaders[];
  mapHeaders: MapJsonKeyHeaders[];
  data: any[];
  onEditCell?: (data: any) => void;
  onDeleteCell?: (data: any) => void;
}

export interface ITableHeaders {
  name: string;
  type: string;
}

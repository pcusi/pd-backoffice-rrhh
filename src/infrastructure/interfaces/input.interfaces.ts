import { ReactNode } from "react";
import { Control, FieldErrors, FieldValues } from "react-hook-form";

export interface InputController {
  errors: FieldErrors;
  control: Control<FieldValues, object>;
  name: string;
  errorMessage: string;
  placeholder: string;
  type: string;
  label: string;
  isComboBox?: boolean;
  renderOptions?: ReactNode;
  ariaLabel?: string;
}

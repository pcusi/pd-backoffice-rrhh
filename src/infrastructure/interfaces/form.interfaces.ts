import {
  Control,
  FieldErrors,
  FieldValues,
  UseFormGetValues,
  UseFormReset,
  UseFormResetField,
  UseFormSetError,
  UseFormSetValue,
} from "react-hook-form";

export interface FormRequest<T extends FieldValues> {
  control: Control<FieldValues | any, Object>;
  errors: FieldErrors<T>;
  resetField: UseFormResetField<T>;
  setValue: UseFormSetValue<T>;
  setError: UseFormSetError<T>;
  reset?: UseFormReset<T>;
  isDirty?: boolean;
  isValid?: boolean;
  getValues?: UseFormGetValues<T>;
}

export enum CustomHeaders {
  TOKEN = "authToken",
  USER = "user",
  NAME = "loggedName",
}

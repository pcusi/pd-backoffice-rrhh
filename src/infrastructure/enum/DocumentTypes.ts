export enum DocumentTypes {
  DNI = "dni",
  RUC = "ruc",
  PASSPORT = "passport",
  FOREIGNER_CARD = "foreigner-card",
}

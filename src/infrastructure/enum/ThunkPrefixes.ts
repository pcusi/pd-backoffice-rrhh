export enum ThunkPrefixes {}

export enum ThunkJobPrefixes {
  JOBS = "jobs",
}

export enum ThunkUserPrefixes {
  USERS = "users",
  SAVE = "user/save",
  UPDATE = "user/update",
}

export enum Texts {
  LOGIN = "Log in",
  WELCOME = "Welcome back,",
  WELCOME_DETAILS = "Welcome, please enter your details.",
}

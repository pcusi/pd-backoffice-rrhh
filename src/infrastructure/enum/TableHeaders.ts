export enum TableHeaders {
  EMAIL = "email",
  NAMES = "names",
  LASTNAMES = "lastnames",
  ROLE = "role",
  DOC_NUMBER = "documentNumber",
  DOC_TYPE = "documentType",
  CREATED_AT = "createdAt",
  ID = "_id",
}

export enum TableJobHeaders {
  NAME = "name",
  VACANCIES = "vacancies",
  ID = "_id",
}

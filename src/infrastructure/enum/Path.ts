export enum Path {
  DASHBOARD = "dashboard",
  USERS = "users",
  JOBS = "jobs",
}

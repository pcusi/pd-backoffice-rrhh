export enum Roles {
  ADMIN = "admin",
  SUPER = "super-admin",
  RESOURCE = "resource",
  EMPLOYEE = "employee",
}

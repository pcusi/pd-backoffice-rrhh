import { useAuthForm } from "@components/auth";
import { CustomHeaders } from "@infrastructure/enum";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { userInitialState } from "@store/reducers/user/user.slice";
import { renderHook } from "@testing-library/react";
import React from "react";
import { act } from "react-dom/test-utils";
import { useWatch } from "react-hook-form";

import * as StoreHooks from "../../../store/hooks/store.hook";

const mockSubmit = jest.fn((data: any) => Promise.resolve(data));

jest.mock("react-hook-form", () => ({
  useForm: jest.fn().mockReturnValue({
    formState: {
      isValid: true,
      errors: () => ({}),
    },
    handleSubmit: () => mockSubmit,
    control: () => ({}),
    watch: () => ({
      subscribe: jest.fn(),
      unsubscribe: jest.fn(),
    }),
  }),
  useWatch: jest.fn(),
}));

jest.mock("../AuthForm/AuthForm", () => ({
  __esModule: true,
  default: () => <div data-testid={"AuthForm"} />,
}));

jest.mock("../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

const setToken = (value: string) => {
  localStorage.setItem(CustomHeaders.TOKEN, value);
};

const setNames = (value: string) => {
  localStorage.setItem(CustomHeaders.NAME, value);
};

describe("useAuthForm tests", () => {
  const renderCustomHook = () => renderHook(() => useAuthForm());
  const mockUseWatch: jest.Mock = useWatch as jest.Mock;

  const mockUseWatchFn = (values: object) => {
    mockUseWatch.mockReturnValue({
      email: values["email"],
      password: values["password"],
    });
  };

  beforeEach(() => {
    mockUseWatchFn({
      email: "pcusir@gmail.com",
      password: "123",
    });
  });

  const mockDispatch = () => {
    const dispatch = jest.fn();

    jest.spyOn(StoreHooks, "useAppDispatch").mockReturnValue(dispatch);

    return dispatch;
  };

  const getItem = jest.spyOn(Storage.prototype, "getItem");
  const setItem = jest.spyOn(Storage.prototype, "setItem");

  const mockSelector = () => {
    jest
      .spyOn(StoreHooks, "useAppSelector")
      // @ts-ignore
      .mockImplementation((state) =>
        state({
          auth: { loading: false, user: authInitialState.user },
          user: { ...userInitialState },
          job: { loading: false, jobs: [] },
        }),
      );
  };

  it("When token is null", async () => {
    const dispatch = mockDispatch();

    mockSelector();
    renderCustomHook();
    expect(dispatch).toHaveBeenCalled();
  });

  it("When component hook is called, it must dispatch once at first", async () => {
    setToken("123");
    const dispatch = mockDispatch();

    mockSelector();
    renderCustomHook();

    getItem.mockReturnValue("123");

    expect(dispatch).toHaveBeenCalled();

    expect(getItem).toHaveBeenCalledTimes(4);

    getItem.mockClear();
  });

  it("should fire an event submit when is valid is true and should dispatch an action", () => {
    setToken("123");
    const dispatch = mockDispatch();

    mockSelector();
    const { result } = renderCustomHook();

    act(() => {
      result.current.onSubmit();
    });

    expect(dispatch).toBeCalled();
  });

  it("should return Piero to prop names in the custom hook", () => {
    setNames("Piero");
    mockSelector();
    renderCustomHook();

    setItem.mockClear();
  });
});

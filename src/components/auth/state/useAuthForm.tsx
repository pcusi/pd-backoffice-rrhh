import { CustomHeaders, FormRequest } from "@core/infrastructure";
import { updateUserForm } from "@store/actions/auth.actions";
import { useAppDispatch, useAppSelector } from "@store/hooks/store.hook";
import { auth } from "@store/thunks/auth/auth.thunks";
import { useEffect, useState } from "react";
import { useForm, useWatch } from "react-hook-form";
import { UserResponse } from "types/user_schema";

export interface UseAuthFormProps extends FormRequest<UserResponse> {
  names: string;
  loading: boolean;
  onSubmit: () => void;
}

export const useAuthForm = (): UseAuthFormProps => {
  const [token] = useState<string | null>(localStorage.getItem(CustomHeaders.TOKEN));
  const names: string | null = localStorage.getItem(CustomHeaders.NAME);
  const { loading, user } = useAppSelector((state) => ({ ...state.auth }));
  const form = useForm<UserResponse>({
    defaultValues: { ...user },
    mode: "onBlur",
    reValidateMode: "onBlur",
  });
  const {
    control,
    resetField,
    reset,
    formState: { errors, isValid },
    setError,
    setValue,
  } = form;
  let fieldsWatch = useWatch({
    control,
  });
  const dispatch = useAppDispatch();

  const onSubmit = () => {
    /* istanbul ignore else */
    if (isValid) {
      dispatch(auth(user));
    }
  };

  useEffect(() => {
    /* istanbul ignore else */
    if (token !== null) {
      window.location.href = "/admin/dashboard";
    }
  }, [token]);

  useEffect(() => {
    let { email, password, ...formState } = fieldsWatch;

    fieldsWatch = {
      ...fieldsWatch,
      email,
      password,
    };

    formState = {
      ...formState,
      ...fieldsWatch,
    };

    dispatch(updateUserForm(formState as UserResponse));
  }, [fieldsWatch]);

  return {
    control,
    errors,
    isValid,
    onSubmit,
    reset,
    resetField,
    setError,
    setValue,
    loading,
    names: names !== null ? names : "",
  };
};

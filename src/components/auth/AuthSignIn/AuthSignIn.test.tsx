import * as UseAuthForm from "@components/index";
import { mockControlHookForm } from "@infrastructure/constants";
import { Texts } from "@infrastructure/enum";
import { render, screen } from "@testing-library/react";
import React from "react";

import { AuthSignIn } from "./AuthSignIn";

const AuthForm = "AuthForm";

jest.mock("../AuthForm/AuthForm", () => ({
  __esModule: true,
  default: () => <div data-testid={AuthForm} />,
}));

jest.mock("../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

jest.mock("../state/useAuthForm", () => ({
  useAuthForm: jest.fn(),
}));

const mockLogin = jest.fn();

describe("AuthSignIn Component Testing", () => {
  const renderComponent = () => {
    return render(<AuthSignIn />);
  };

  const mockUseAuthForm = (names: string) => {
    jest.spyOn(UseAuthForm, "useAuthForm").mockReturnValue({
      errors: {},
      control: mockControlHookForm<any>({}, {}, {}),
      onSubmit: mockLogin,
      isValid: true,
      loading: false,
      getValues: jest.fn(),
      reset: jest.fn(),
      setValue: jest.fn(),
      resetField: jest.fn(),
      setError: jest.fn(),
      names,
    });
  };

  it("should render the sign in component", () => {
    mockUseAuthForm("");
    renderComponent();
    expect(screen.getByTestId(AuthForm)).toBeInTheDocument();
  });

  it("should render the text 'Welcome, please enter your details' when custom hook prop names is empty", () => {
    mockUseAuthForm("");
    const { getByText } = renderComponent();

    expect(getByText(Texts.WELCOME_DETAILS)).toBeInTheDocument();
  });

  it("should render the text 'Welcome, ${name}' when custom hook prop names isn't empty", () => {
    const names: string = "Piero";
    mockUseAuthForm(names);
    const { getByText } = renderComponent();

    expect(getByText(`${Texts.WELCOME} ${names}`)).toBeInTheDocument();
  });
});

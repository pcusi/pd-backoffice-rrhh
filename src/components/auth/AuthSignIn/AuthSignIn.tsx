import { authStyles, useAuthForm } from "@components/auth";
import AuthForm from "@components/auth/AuthForm/AuthForm";
import { Texts } from "@infrastructure/enum";
import { Box, Grid, Typography } from "@mui/material";
import React from "react";

export const AuthSignIn = () => {
  const { control, errors, onSubmit, isValid, loading, names } = useAuthForm();

  return (
    <Box sx={authStyles.container}>
      <Grid container sx={authStyles.grid}>
        <Grid item xs={4} sx={authStyles.gridLeft}>
          <Typography variant="h1" color="primary.dark" sx={authStyles.title}>
            {Texts.LOGIN}
          </Typography>
          <Typography variant="h5" color="text.dark" sx={authStyles.subtitle}>
            {names !== "" ? Texts.WELCOME + ` ${names}` : Texts.WELCOME_DETAILS}
          </Typography>
          <AuthForm
            control={control}
            errors={errors}
            onSubmit={onSubmit}
            isValid={isValid}
            loading={loading}
          />
        </Grid>
        <Grid item xs={8} sx={authStyles.gridRight}>
          <img src="/auth_background.svg" alt="Auth Background" />
        </Grid>
      </Grid>
    </Box>
  );
};

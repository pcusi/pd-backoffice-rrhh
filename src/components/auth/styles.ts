import { StylesProps } from "@shared/styles.props";

export const authStyles: StylesProps = {
  container: {
    width: "100%",
    height: "100vh",
  },
  title: {
    fontWeight: 700,
    mb: 1,
  },
  subtitle: {
    mb: 4,
  },
  grid: {
    height: "100%",
  },
  gridLeft: {
    padding: 6,
  },
  gridRight: {
    width: "100%",
    height: "100%",
    img: {
      borderTopLeftRadius: 64,
      borderBottomLeftRadius: 64,
      width: "100%",
      height: "100%",
      objectFit: "cover",
    },
  },
};

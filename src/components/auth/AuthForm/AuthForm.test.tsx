import { AuthFormProps } from "@components/auth";
import AuthForm from "@components/auth/AuthForm/AuthForm";
import { mockControlHookForm } from "@infrastructure/constants";
import { render } from "@testing-library/react";
import React from "react";
import { FieldErrors } from "react-hook-form";
import { UserResponse } from "types/user_schema";

describe("AuthForm Component Testing", () => {
  let props: AuthFormProps;
  let errors: FieldErrors<UserResponse>;

  beforeEach(() => {
    errors = {
      email: {
        type: "required",
        message: "email error",
      },
    };

    props = {
      control: mockControlHookForm<any>({}, {}, {}),
      errors,
      reset: jest.fn(),
      resetField: jest.fn(),
      isValid: false,
      loading: false,
      onSubmit: jest.fn(),
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });

  const renderComponent = () => {
    return render(<AuthForm {...props} />);
  };

  it("should render AuthForm component", () => {
    renderComponent();
  });
});

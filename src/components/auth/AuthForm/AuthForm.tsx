import { Input } from "@core/shared";
import { authLabels, authPlaceholder } from "@infrastructure/constants";
import { LoadingButton } from "@mui/lab";
import { Grid } from "@mui/material";
import { FC } from "react";
import {
  Control,
  FieldErrors,
  FieldValues,
  UseFormReset,
  UseFormResetField,
} from "react-hook-form";
import { UserResponse } from "types/user_schema";

export interface AuthFormProps {
  loading: boolean;
  onSubmit: () => void;
  control: Control<FieldValues | any, Object>;
  errors: FieldErrors<UserResponse>;
  resetField?: UseFormResetField<UserResponse>;
  reset?: UseFormReset<UserResponse>;
  isValid?: boolean;
}

const AuthForm: FC<AuthFormProps> = ({
  onSubmit,
  control,
  errors,
  isValid,
  loading,
}) => {
  return (
    <form>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Input
            label={authLabels.email}
            control={control}
            errors={errors}
            name="email"
            type="email"
            placeholder={authPlaceholder.email}
            errorMessage={"Email not match"}
          />
        </Grid>
        <Grid item xs={12}>
          <Input
            label={authLabels.password}
            control={control}
            errors={errors}
            type="password"
            name="password"
            placeholder={authPlaceholder.password}
            errorMessage={"Password not match"}
          />
        </Grid>
        <Grid item xs={12}>
          <LoadingButton
            loadingPosition="end"
            endIcon={<></>}
            disabled={!isValid}
            fullWidth
            variant="contained"
            onClick={onSubmit}
            type="button"
            loading={loading}
          >
            {authLabels.submit}
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default AuthForm;

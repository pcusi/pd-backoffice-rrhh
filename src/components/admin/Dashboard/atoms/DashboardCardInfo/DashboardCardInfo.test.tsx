import { DashboardCardInfo, DashboardCardInfoProps } from "@core/components";
import { render } from "@testing-library/react";
import React from "react";

describe("DashboardCardInfo Testing", () => {
  let props: DashboardCardInfoProps;

  beforeEach(() => {
    props = {
      title: "Piero in Dashboard Card Info",
    };
  });

  const renderComponent = () => {
    return render(<DashboardCardInfo {...props} />);
  };

  it("must render the title correctly", () => {
    const { getByText } = renderComponent();

    expect(getByText("Piero in Dashboard Card Info")).toContainHTML(
      "Piero in Dashboard Card Info",
    );
  });
});

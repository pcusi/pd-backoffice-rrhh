import { Card, Stack, Typography } from "@mui/material";
import { FC } from "react";

export interface DashboardCardInfoProps {
  title: string;
}

export const DashboardCardInfo: FC<DashboardCardInfoProps> = ({ title }) => {
  return (
    <Card>
      <Stack direction="row" justifyContent="space-between">
        <Typography variant="h1">{title}</Typography>
      </Stack>
    </Card>
  );
};

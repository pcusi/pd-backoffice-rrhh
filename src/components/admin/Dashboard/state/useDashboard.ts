import { useAppSelector } from "@store/hooks/store.hook";
import { UserResponse } from "types/user_schema";

export interface UseDashboardProps {
  users: UserResponse[];
}

export const useDashboard = (): UseDashboardProps => {
  const { users } = useAppSelector((state) => ({ ...state.user }));

  return {
    users,
  };
};

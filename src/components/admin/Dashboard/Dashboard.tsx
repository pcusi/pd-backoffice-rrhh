import { useDashboard } from "@core/components";
import { dashboardLabels } from "@core/infrastructure";
import { Grid, Typography } from "@mui/material";
import React from "react";

export const Dashboard = () => {
  const { users } = useDashboard();

  console.log({ users });

  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h1" color="text.dark">
          {dashboardLabels.title}
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Grid container spacing={2}>
          <Grid item xs={6}></Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

import { Layouts, MediaQuery } from "@infrastructure/enum";
import { StylesProps } from "@shared/styles.props";
import { Palette } from "@themes/theme";

export const navbarStyles: StylesProps = {
  navbar: {
    position: "fixed",
    top: 0,
    left: Layouts.SIDEBAR,
    width: `calc(100% - ${Layouts.SIDEBAR})`,
    backgroundColor: Palette.text.glass,
    padding: 2,
    zIndex: 99,
    backdropFilter: "blur(5px)",
    [MediaQuery.MOBILE]: {
      position: "relative",
      width: "100%",
      left: 0,
    },
  },
  avatar: {
    width: "30px",
    height: "30px",
    borderRadius: 1,
    cursor: "pointer",
  },
};

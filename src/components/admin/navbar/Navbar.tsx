import { navbarStyles } from "@components/admin";
import { adminLabels } from "@core/infrastructure";
import { rolesCatalog } from "@infrastructure/catalog";
import { Avatar, Badge, Box, Popover, Stack, Typography } from "@mui/material";
import { get } from "lodash";
import { FC, MouseEvent } from "react";
import { MdNotifications } from "react-icons/md";
import { UserResponse } from "types/user_schema";

export interface NavbarProps {
  user: UserResponse;
  anchorEl: HTMLDivElement | null;
  open: boolean;
  popoverClose: () => void;
  popoverClick: (event: MouseEvent<HTMLDivElement>) => void;
  notificationsLength: number;
  logout: () => void;
}

export const Navbar: FC<NavbarProps> = ({
  user,
  anchorEl,
  open,
  popoverClick,
  popoverClose,
  logout,
  notificationsLength,
}) => {
  return (
    <Box sx={navbarStyles.navbar}>
      <Stack direction="row" spacing={2} justifyContent="space-between">
        <Stack></Stack>
        <Stack direction="row" alignItems="center" spacing={2}>
          <Stack direction="column" spacing={0.5}>
            <Typography variant="body2" color="text.dark" textAlign="right">
              {get(user, "names")}
            </Typography>
            <Typography variant="caption" color="text.dark" textAlign="right">
              {rolesCatalog[get(user, "role", "")]}
            </Typography>
          </Stack>
          <Avatar
            src=""
            variant="rounded"
            onClick={popoverClick}
            sx={navbarStyles.avatar}
          />
          <Popover
            open={open}
            anchorEl={anchorEl}
            onClose={popoverClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
          >
            <Typography variant="body1" color="text.dark" onClick={logout}>
              {adminLabels.logout}
            </Typography>
          </Popover>
          <Badge badgeContent={notificationsLength} color="primary">
            <MdNotifications></MdNotifications>
          </Badge>
        </Stack>
      </Stack>
    </Box>
  );
};

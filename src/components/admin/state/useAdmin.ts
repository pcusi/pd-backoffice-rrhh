import { NavbarProps } from "@components/admin";
import { CustomHeaders } from "@infrastructure/enum";
import { getSocket, initSocket } from "@shared/utils";
import { MouseEvent, useEffect, useState } from "react";
import { Socket } from "socket.io-client";
import { NotificationResponse } from "types/notification_schema";
import { UserResponse } from "types/user_schema";

export interface UseAdminProps extends NavbarProps {}

export const useAdmin = (): UseAdminProps => {
  const user: UserResponse = JSON.parse(
    `${localStorage.getItem(CustomHeaders.USER)}`,
  );

  const [anchorEl, setAnchorEl] = useState<HTMLDivElement | null>(null);
  const [notificationsLength, setNotificationsLength] = useState<number>(0);

  const handleClick = (event: MouseEvent<HTMLDivElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const logout = () => {
    localStorage.removeItem(CustomHeaders.TOKEN);
    localStorage.removeItem(CustomHeaders.USER);
    window.location.href = "/auth";
  };

  const pagesRefresh = (socket: Socket) => {
    /* istanbul ignore else */
    if (window.performance) {
      /* istanbul ignore else */
      if (performance.navigation.type == 1) {
        socket.on("message", (data) => {
          const notifications: NotificationResponse[] = JSON.parse(data);

          setNotificationsLength(notifications.length);
        });
      }
      socket.off("message");
      socket.disconnect();
    }
  };

  useEffect(() => {
    initSocket();
  }, []);

  useEffect(() => {
    const socket = getSocket();

    pagesRefresh(socket);
  }, []);

  return {
    user,
    popoverClick: handleClick,
    popoverClose: handleClose,
    open,
    logout,
    anchorEl,
    notificationsLength,
  };
};

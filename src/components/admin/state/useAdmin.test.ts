import { useAdmin } from "@core/components";
import { mock_notifications } from "@core/infrastructure";
import { renderHook } from "@testing-library/react";
import { MouseEvent } from "react";
import { act } from "react-dom/test-utils";
import { Socket } from "socket.io-client";

const socketOnMock = jest.fn();
const socketOffMock = jest.fn();
const socketEmitMock = jest.fn();
const socketDisconnect = jest.fn();

const mockedSocket: Socket = {
  on: socketOnMock.mockResolvedValue([...mock_notifications]),
  off: socketOffMock,
  emit: socketEmitMock,
  disconnect: socketDisconnect,
} as any;

jest.mock("socket.io-client", () => {
  return {
    io: jest.fn(() => mockedSocket),
  };
});

describe("Custom Hook useAdmin Testing", () => {
  function mockPerformance(type: number) {
    return Object.defineProperty(window, "performance", {
      value: {
        navigation: {
          type: type,
        },
      },
    });
  }

  const renderCustomHook = () => renderHook(() => useAdmin());

  it("should mock the custom hook properly", () => {
    mockPerformance(1);
    renderCustomHook();
  });

  it("should socket off and disconnect when window.performance.navigation.type is equal to zero", () => {
    mockPerformance(255);
    renderCustomHook();
  });

  it("should mock socket.on and data response", () => {
    mockPerformance(1);
    const { result } = renderCustomHook();

    expect(socketOnMock).toHaveBeenCalledWith("message", expect.any(Function));

    const data = JSON.stringify([{ userId: "1", name: "1" }]);
    const callback = socketOnMock.mock.calls[0][1];
    callback(data);

    expect(result.current.notificationsLength).toEqual(0);
    expect(socketOnMock).toHaveBeenCalledTimes(3);
  });

  it("should click handleClose", () => {
    mockPerformance(1);
    const { result } = renderCustomHook();

    act(() => {
      result.current.popoverClose();
    });
  });

  it("should click logout", () => {
    mockPerformance(1);
    const { result } = renderCustomHook();

    act(() => {
      result.current.logout();
    });
  });

  it("should click popoverClick", () => {
    mockPerformance(1);
    const { result } = renderCustomHook();
    const event = {
      currentTarget: { id: "div1" },
    } as MouseEvent<HTMLDivElement>;

    act(() => {
      result.current.popoverClick(event);
    });
  });
});

import UserForm from "@components/admin/Users/atoms/UserForm/UserForm";
import { useUser } from "@core/components";
import { userLabels } from "@core/infrastructure";
import { CustomTable } from "@core/shared";
import { Grid } from "@mui/material";
import React from "react";

export const Users = () => {
  const { users, headers, mapHeaders, onEditCell, open, onAddCell, loading } =
    useUser();

  return (
    <>
      <UserForm open={open} closeDialog={onAddCell} loading={loading} />
      <Grid container>
        <Grid item xs={12}>
          <CustomTable
            buttonTitle={userLabels.add}
            title={userLabels.title}
            headers={headers}
            data={users}
            mapHeaders={mapHeaders}
            onAddCell={onAddCell}
            onEditCell={onEditCell}
          />
        </Grid>
      </Grid>
    </>
  );
};

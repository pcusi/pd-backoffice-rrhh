import UserForm, {
  UserFormProps,
} from "@components/admin/Users/atoms/UserForm/UserForm";
import {
  mockControlHookForm,
  userLabels,
  user_form_values,
} from "@core/infrastructure";
import * as StoreHooks from "@store/hooks/store.hook";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { jobInitialState } from "@store/reducers/recruitment/job/job.slice";
import { userInitialState } from "@store/reducers/user/user.slice";
import { render } from "@testing-library/react";
import { FieldErrors } from "react-hook-form";
import { UserResponse } from "types/user_schema";

jest.mock("../../../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

describe("UserForm Testing", () => {
  let props: UserFormProps;
  let errors: FieldErrors<UserResponse>;
  const mockSelector = (userForm?: UserResponse) => {
    jest
      .spyOn(StoreHooks, "useAppSelector")
      // @ts-ignore
      .mockImplementation((state) =>
        state({
          auth: { ...authInitialState },
          user: { ...userInitialState, userForm },
          job: { ...jobInitialState },
        }),
      );
  };

  beforeEach(() => {
    errors = {
      email: {
        type: "required",
        message: "email error",
      },
    };
    props = {
      open: true,
      isValid: false,
      control: mockControlHookForm<any>({}, {}, {}),
      errors,
      loading: false,
      closeDialog: jest.fn(),
    };
  });

  const renderComponent = () => {
    return render(<UserForm {...props} />);
  };

  it("when userForm is undefined should return the", () => {
    mockSelector(undefined);
    renderComponent();
  });

  it("when useForm have data should return the values stored in the redux state", () => {
    mockSelector({ ...user_form_values });
    const { getByText } = renderComponent();

    expect(getByText(userLabels.dialogTitle)).toContainHTML(userLabels.dialogTitle);
  });
});

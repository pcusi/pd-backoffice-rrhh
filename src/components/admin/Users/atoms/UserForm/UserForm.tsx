import { useUserForm } from "@core/components";
import {
  TableHeaders,
  document_type_catalog,
  document_types,
  roles,
  rolesCatalog,
  userLabels,
} from "@core/infrastructure";
import { Input, ModalForm } from "@core/shared";
import { Grid, MenuItem, Typography } from "@mui/material";
import { FC } from "react";
import {
  Control,
  FieldErrors,
  FieldValues,
  UseFormReset,
  UseFormResetField,
} from "react-hook-form";
import { UserResponse } from "types/user_schema";

export interface UserFormProps {
  open: boolean;
  closeDialog: () => void;
  loading: boolean;
  control?: Control<FieldValues | any, Object>;
  errors?: FieldErrors<UserResponse>;
  resetField?: UseFormResetField<UserResponse>;
  reset?: UseFormReset<UserResponse>;
  isValid?: boolean;
}

const UserForm: FC<UserFormProps> = ({ closeDialog, open, loading }) => {
  const { isValid, errors, onSubmit, control } = useUserForm();

  return (
    <ModalForm
      closeDialog={closeDialog}
      dialogTitle={userLabels.dialogTitle}
      buttonTitle={userLabels.dialogSubmitTitle}
      isValid={isValid}
      open={open}
      onSubmit={onSubmit}
      children={
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Input
              ariaLabel="lbl-names"
              label="Names"
              type="text"
              control={control}
              errors={errors}
              placeholder=""
              errorMessage=""
              name={TableHeaders.NAMES}
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              label="Lastnames"
              type="text"
              ariaLabel="Lastnames"
              control={control}
              errors={errors}
              placeholder=""
              errorMessage=""
              name={TableHeaders.LASTNAMES}
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              label="Document Type"
              type="text"
              control={control}
              errors={errors}
              placeholder=""
              errorMessage=""
              isComboBox={true}
              renderOptions={document_types.map((doc: string) => (
                <MenuItem value={doc} key={Math.random()}>
                  <Typography variant="body1" color="text.dark">
                    {document_type_catalog[doc]}
                  </Typography>
                </MenuItem>
              ))}
              name={TableHeaders.DOC_TYPE}
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              label="Document Number"
              type="text"
              control={control}
              errors={errors}
              placeholder="12345678"
              errorMessage=""
              name={TableHeaders.DOC_NUMBER}
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              label="Roles"
              type="text"
              control={control}
              errors={errors}
              placeholder=""
              errorMessage=""
              isComboBox={true}
              renderOptions={roles.map((role: string) => (
                <MenuItem value={role} key={Math.random()}>
                  <Typography variant="body1" color="text.dark">
                    {rolesCatalog[role]}
                  </Typography>
                </MenuItem>
              ))}
              name={TableHeaders.ROLE}
            />
          </Grid>
          <Grid item xs={6}>
            <Input
              label="Email"
              type="text"
              control={control}
              errors={errors}
              placeholder="example@gmail.com"
              errorMessage=""
              name={TableHeaders.EMAIL}
            />
          </Grid>
        </Grid>
      }
      loading={loading}
    />
  );
};

export default UserForm;

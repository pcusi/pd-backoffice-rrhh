import { useUserForm } from "@components/admin";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { userInitialState } from "@store/reducers/user/user.slice";
import { renderHook } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { useWatch } from "react-hook-form";

import * as StoreHooks from "../../../../../../store/hooks/store.hook";

jest.mock("react-hook-form", () => ({
  useForm: jest.fn().mockReturnValue({
    formState: {
      isValid: true,
      errors: () => ({}),
    },
    reset: jest.fn(),
    control: () => ({}),
    watch: () => ({
      subscribe: jest.fn(),
      unsubscribe: jest.fn(),
    }),
  }),
  useWatch: jest.fn(),
}));

jest.mock("../../../../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

describe("useUserForm tests", () => {
  const renderCustomHook = () => renderHook(() => useUserForm());
  const mockUseWatch: jest.Mock = useWatch as jest.Mock;

  const mockUseWatchFn = (values: object) => {
    mockUseWatch.mockReturnValue({
      lastnames: values["lastnames"],
    });
  };

  beforeEach(() => {
    mockUseWatchFn({
      lastnames: "pcusir@gmail.com",
    });
  });

  const mockDispatch = () => {
    const dispatch = jest.fn();

    jest.spyOn(StoreHooks, "useAppDispatch").mockReturnValue(dispatch);

    return dispatch;
  };

  const mockSelector = () => {
    jest
      .spyOn(StoreHooks, "useAppSelector")
      // @ts-ignore
      .mockImplementation((state) =>
        state({
          auth: { loading: false, user: authInitialState.user },
          user: { ...userInitialState },
          job: { loading: false, jobs: [] },
        }),
      );
  };

  it("should execute onSubmit", () => {
    mockSelector();
    const dispatch = mockDispatch();
    const { result } = renderCustomHook();

    act(() => {
      result.current.onSubmit();
    });

    expect(dispatch).toHaveBeenCalledTimes(1);
  });
});

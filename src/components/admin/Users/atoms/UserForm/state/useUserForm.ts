import { FormRequest, user_form_values } from "@core/infrastructure";
import { useAppDispatch, useAppSelector } from "@store/hooks/store.hook";
import { getUsers, updateUser } from "@store/thunks/user/user.thunks";
import { useEffect, useState } from "react";
import { useForm, useWatch } from "react-hook-form";
import { UserResponse } from "types/user_schema";

export interface UseUserFormProps extends FormRequest<UserResponse> {
  onSubmit: () => void;
}

export const useUserForm = (): UseUserFormProps => {
  const { userForm } = useAppSelector((state) => ({ ...state.user }));
  const dispatch = useAppDispatch();
  const [requestForm, setRequestForm] = useState<UserResponse>({
    ...user_form_values,
  });

  const form = useForm<UserResponse>({
    defaultValues: {
      email: userForm ? userForm.email : user_form_values.email,
      names: userForm ? userForm.names : user_form_values.names,
      lastnames: userForm ? userForm.lastnames : user_form_values.lastnames,
      documentType: userForm ? userForm.documentType : user_form_values.documentType,
      role: userForm ? userForm.role : user_form_values.role,
    },
    mode: "onBlur",
    reValidateMode: "onChange",
  });

  const {
    control,
    resetField,
    reset,
    formState: { errors, isValid },
    setError,
    setValue,
  } = form;
  let fieldsWatch = useWatch({
    control,
  });

  const onSubmit = async () => {
    await dispatch(updateUser(requestForm));
    await dispatch(getUsers());
    reset({ ...user_form_values });
  };

  useEffect(() => {
    let { ...formState } = fieldsWatch;

    fieldsWatch = {
      ...fieldsWatch,
    };

    formState = {
      ...formState,
      ...fieldsWatch,
    };

    setRequestForm(formState as UserResponse);
  }, [fieldsWatch]);

  useEffect(() => {
    if (userForm !== undefined) {
      reset(userForm);
    }
  }, [userForm]);

  return {
    control,
    errors,
    isValid,
    onSubmit,
    reset,
    resetField,
    setError,
    setValue,
  };
};

import { useUser } from "@core/components";
import { Countries, mock_users } from "@core/infrastructure";
import * as StoreHooks from "@store/hooks/store.hook";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { renderHook } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { UserResponse } from "types/user_schema";

jest.mock("../../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

describe("Custom Hook useAdmin Testing", () => {
  const renderCustomHook = () => renderHook(() => useUser());

  const mockDispatch = () => {
    const dispatch = jest.fn();

    jest.spyOn(StoreHooks, "useAppDispatch").mockReturnValue(dispatch);

    return dispatch;
  };

  const mockSelector = (users?: UserResponse[]) => {
    jest
      .spyOn(StoreHooks, "useAppSelector")
      // @ts-ignore
      .mockImplementation((state) =>
        state({
          auth: { loading: false, user: authInitialState.user },
          user: {
            loading: false,
            users: users ? users : [],
            modal: false,
            userForm: undefined,
          },
          job: { loading: false, jobs: [] },
        }),
      );
  };

  beforeEach(() => {
    mockSelector();
  });

  it("shouldn't dispatch if user slice prop 'users' have values stored", () => {
    const dispatch = mockDispatch();
    mockSelector([{ ...mock_users }]);
    renderCustomHook();
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  it("should open the user form modal", () => {
    const dispatch = mockDispatch();
    const { result } = renderCustomHook();

    act(() => {
      result.current.onAddCell();
    });

    expect(dispatch).toHaveBeenCalledTimes(2);
  });

  it("should mock the custom hook properly", () => {
    const dispatch = mockDispatch();
    renderCustomHook();
    expect(dispatch).toHaveBeenCalledTimes(1);
  });

  it("should dispatch get users when 'users' array is equal to zero", () => {
    const dispatch = mockDispatch();
    renderCustomHook();
    expect(dispatch).toHaveBeenCalledTimes(1);
  });

  it("should dispatch two times when user form submit was clicked", () => {
    const dispatch = mockDispatch();
    const { result } = renderCustomHook();
    const user: UserResponse = {
      names: "",
      lastnames: "",
      documentNumber: "",
      country: Countries.peru,
      email: "",
    };

    act(() => {
      result.current.onEditCell(user);
    });

    expect(dispatch).toBeCalledTimes(3);
  });
});

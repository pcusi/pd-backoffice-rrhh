import {
  ITableHeaders,
  MapJsonKeyHeaders,
  map_user_headers,
  table_user_headers,
} from "@core/infrastructure";
import { switchModal, updateUserForm } from "@store/actions/user.actions";
import { useAppDispatch, useAppSelector } from "@store/hooks/store.hook";
import { getUsers } from "@store/thunks/user/user.thunks";
import { useEffect } from "react";
import { UserResponse } from "types/user_schema";

export interface UseUserProps {
  users: UserResponse[];
  headers: ITableHeaders[];
  mapHeaders: MapJsonKeyHeaders[];
  onEditCell: (data: any) => void;
  open: boolean;
  onAddCell: () => void;
  loading: boolean;
  onDeleteCell?: (data: any) => void;
}

export const useUser = (): UseUserProps => {
  const { users, loading, modal } = useAppSelector((state) => ({ ...state.user }));
  const dispatch = useAppDispatch();

  const fetchUsers = () => {
    dispatch(getUsers());
  };

  const editUser = (data: UserResponse) => {
    dispatch(updateUserForm(data));
    dispatch(switchModal(!modal));
  };

  const switchDialog = () => {
    dispatch(switchModal(!modal));
  };

  useEffect(() => {
    if (users.length === 0) {
      fetchUsers();
    }

    return;
  }, [users]);

  return {
    users,
    headers: table_user_headers,
    mapHeaders: map_user_headers,
    onEditCell: editUser,
    open: modal,
    loading,
    onAddCell: switchDialog,
  };
};

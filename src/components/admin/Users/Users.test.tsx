import * as UseUserState from "@core/components";
import { Users } from "@core/components";
import {
  Roles,
  TableCellType,
  TableHeaders,
  mock_users,
  userLabels,
  user_form_values,
} from "@core/infrastructure";
import * as StoreHooks from "@store/hooks/store.hook";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { jobInitialState } from "@store/reducers/recruitment/job/job.slice";
import { userInitialState } from "@store/reducers/user/user.slice";
import { render } from "@testing-library/react";
import React from "react";
import { UserResponse } from "types/user_schema";

jest.mock("./state/useUser", () => ({
  useUser: jest.fn(),
}));

jest.mock("../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

describe("User Testing", () => {
  const mockSelector = (userForm?: UserResponse) => {
    jest
      .spyOn(StoreHooks, "useAppSelector")
      // @ts-ignore
      .mockImplementation((state) =>
        state({
          auth: { ...authInitialState },
          user: { ...userInitialState, userForm },
          job: { ...jobInitialState },
        }),
      );
  };

  const mockUserState = () =>
    jest.spyOn(UseUserState, "useUser").mockReturnValue({
      users: [{ ...mock_users }, { ...mock_users, role: Roles.SUPER }],
      headers: [
        { name: "email", type: TableCellType.NORMAL },
        { name: "names", type: TableCellType.NORMAL },
        { name: "role", type: TableCellType.NORMAL },
      ],
      mapHeaders: [
        {
          keys: [{ key: TableHeaders.EMAIL }],
        },
        {
          keys: [{ key: TableHeaders.NAMES }],
        },
        {
          keys: [{ key: TableHeaders.ROLE }],
        },
      ],
      onEditCell: jest.fn(),
      open: false,
      loading: false,
      onAddCell: jest.fn(),
    });

  const renderComponent = () => {
    return render(<Users />);
  };

  it("should render the User component", () => {
    mockUserState();
    mockSelector({ ...user_form_values });
    const { getByText } = renderComponent();
    expect(getByText(userLabels.title)).toContainHTML(userLabels.title);
  });
});

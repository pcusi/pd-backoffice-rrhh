import { StylesProps } from "@shared/styles.props";
import { Palette } from "@themes/theme";

export const navigationStyles: StylesProps = {
  item: {
    ".icon.active": {
      fontSize: Palette.fontSize.h2,
      padding: 0.25,
      color: Palette.text.white,
      borderRadius: 2,
    },
    ".icon": {
      fontSize: Palette.fontSize.h2,
      padding: 0.25,
      borderRadius: 2,
    },
    ".tab-active": {
      color: Palette.text.white,
      backgroundColor: "secondary.main",
      borderRadius: 2,
      py: 0.5,
      px: 2,
      ".MuiTypography-body1": {
        textDecoration: "none",
      },
    },
    ".tab-non-active": {
      color: Palette.text.dark,
      py: 0.5,
      px: 2,
    },
    ".link-active": {
      textDecoration: "none",
    },
    ".link-non-active": {
      textDecoration: "none",
    },
  },
};

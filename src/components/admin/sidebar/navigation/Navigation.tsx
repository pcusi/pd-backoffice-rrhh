import { navigationStyles } from "@components/admin";
import { NavigationRole, routes_catalog } from "@core/infrastructure";
import { Grid, Stack, Typography } from "@mui/material";
import React, { FC } from "react";
import { NavLink } from "react-router-dom";

export interface NavigationProps {
  menu: NavigationRole[];
}

export const Navigation: FC<NavigationProps> = ({ menu }) => {
  return (
    <Grid container spacing={2}>
      {menu.map(({ Icon, to }) => (
        <Grid item xs={12} key={Math.random()} sx={navigationStyles.item}>
          <NavLink
            to={to}
            className={({ isActive }) =>
              /* istanbul ignore next */ isActive ? "link-active" : "link-non-active"
            }
            /* istanbul ignore next */
            children={({ /* istanbul ignore next */ isActive }) => (
              <Stack
                direction="row"
                alignItems="center"
                spacing={2}
                className={
                  /* istanbul ignore next */ isActive
                    ? "tab-active"
                    : "tab-non-active"
                }
              >
                <Icon
                  className={
                    /* istanbul ignore next */ isActive ? "icon active" : "icon"
                  }
                />
                <Typography variant="body1">{routes_catalog[to]}</Typography>
              </Stack>
            )}
          />
        </Grid>
      ))}
    </Grid>
  );
};

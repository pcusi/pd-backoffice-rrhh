import { Navigation } from "@core/components";
import { navigation_roles } from "@infrastructure/constants";
import { fireEvent, render } from "@testing-library/react";
import React from "react";
import { Route, Routes } from "react-router";
import { MemoryRouter, NavLink } from "react-router-dom";

describe("Navigation Component Testing", () => {
  const renderComponent = () => {
    return render(
      <MemoryRouter initialEntries={["/admin"]}>
        <NavLink to="/admin/dashboard" end className="link-active">
          Home
        </NavLink>
        <Routes>
          <Route path="/admin" element={<Navigation menu={navigation_roles} />} />
        </Routes>
      </MemoryRouter>,
    );
  };

  it("should render the navigation component", () => {
    const { getByRole } = renderComponent();

    fireEvent.click(
      getByRole("link", {
        name: /dashboard/i,
      }),
    );
  });
});

import { NavigationRole, navigation_roles } from "@infrastructure/constants";
import { CustomHeaders } from "@infrastructure/enum";
import { useEffect, useState } from "react";
import { UserResponse } from "types/user_schema";

export interface UseSidebarProps {
  menu: NavigationRole[];
}

export const useSidebar = (): UseSidebarProps => {
  const user: UserResponse = JSON.parse(
    `${localStorage.getItem(CustomHeaders.USER)}`,
  );
  const [menu, setMenu] = useState<NavigationRole[]>([]);

  useEffect(() => {
    if (user) {
      setMenu(
        navigation_roles.filter((item: NavigationRole) =>
          item.roles.some((role) => role === user.role),
        ),
      );
    }
  }, []);

  return {
    menu,
  };
};

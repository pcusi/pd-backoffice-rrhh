import { useSidebar } from "@core/components";
import { CustomHeaders, Roles } from "@infrastructure/enum";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { renderHook } from "@testing-library/react";
import { UserResponse } from "types/user_schema";

describe("Custom Hook useSidebar Testing", () => {
  const renderCustomHook = () => renderHook(() => useSidebar());
  let user: Partial<UserResponse>;

  const mockSetItem = jest.spyOn(Storage.prototype, "setItem");

  beforeEach(() => {
    user = {
      ...authInitialState.user,
      role: Roles.SUPER,
    };
    localStorage.setItem(CustomHeaders.USER, JSON.stringify(user));
  });

  it("should mock the custom hook properly", () => {
    renderCustomHook();
    mockSetItem.mockClear();
  });

  it("should mock the custom hook properly", () => {
    renderCustomHook();

    expect(mockSetItem).toBeCalledTimes(1);

    mockSetItem.mockClear();
  });
});

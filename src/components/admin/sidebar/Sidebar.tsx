import { Navigation, sidebarStyles, useSidebar } from "@components/admin";
import { Card } from "@mui/material";
import React from "react";

export const Sidebar = () => {
  const { menu } = useSidebar();

  return (
    <Card sx={sidebarStyles.sidebar} elevation={0}>
      <Navigation menu={menu} />
    </Card>
  );
};

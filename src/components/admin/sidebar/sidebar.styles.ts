import { Layouts, MediaQuery } from "@infrastructure/enum";
import { StylesProps } from "@shared/styles.props";

export const sidebarStyles: StylesProps = {
  sidebar: {
    position: "fixed",
    height: "100vh",
    borderRadius: 0,
    padding: 4,
    top: 0,
    zIndex: 99,
    width: Layouts.SIDEBAR,
    maxWidth: Layouts.SIDEBAR,
    backgroundColor: "background.primary",
    [MediaQuery.MOBILE]: {
      height: "auto",
    },
  },
};

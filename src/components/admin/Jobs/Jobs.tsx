import { useJob } from "@core/components";
import { jobLabels } from "@core/infrastructure";
import { CustomTable } from "@core/shared";
import { Grid } from "@mui/material";

export const Jobs = () => {
  const { jobs, headers, mapHeaders } = useJob();

  return (
    <Grid container>
      <Grid item xs={12}>
        <CustomTable
          buttonTitle={jobLabels.add}
          title={jobLabels.title}
          data={jobs}
          headers={headers}
          mapHeaders={mapHeaders}
        />
      </Grid>
    </Grid>
  );
};

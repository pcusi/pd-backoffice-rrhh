import {
  ITableHeaders,
  MapJsonKeyHeaders,
  map_job_headers,
  table_job_headers,
} from "@core/infrastructure";
import { useAppDispatch, useAppSelector } from "@store/hooks/store.hook";
import { getJobs } from "@store/thunks/recruitment/jobs/jobs.thunks";
import { useEffect } from "react";
import { JobResponse } from "types/job_schema";

export interface UseJobProps {
  jobs: JobResponse[];
  headers: ITableHeaders[];
  mapHeaders: MapJsonKeyHeaders[];
  onEditCell?: (data: any) => void;
  onDeleteCell?: (data: any) => void;
}

export const useJob = (): UseJobProps => {
  const { jobs } = useAppSelector((state) => ({ ...state.job }));
  const dispatch = useAppDispatch();

  const fetchJobs = () => {
    dispatch(getJobs());
  };

  useEffect(() => {
    if (jobs.length === 0) {
      fetchJobs();
    }

    return;
  }, [jobs]);

  return {
    jobs,
    headers: table_job_headers,
    mapHeaders: map_job_headers,
  };
};

import { useJob } from "@core/components";
import { mock_jobs } from "@core/infrastructure";
import * as StoreHooks from "@store/hooks/store.hook";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { userInitialState } from "@store/reducers/user/user.slice";
import { renderHook } from "@testing-library/react";
import { JobResponse } from "types/job_schema";

jest.mock("../../../../store/hooks/store.hook", () => ({
  useAppDispatch: jest.fn(),
  useAppSelector: jest.fn(),
}));

describe("Custom Hook useJob Testing", () => {
  const renderCustomHook = () => renderHook(() => useJob());

  const mockDispatch = () => {
    const dispatch = jest.fn();

    jest.spyOn(StoreHooks, "useAppDispatch").mockReturnValue(dispatch);

    return dispatch;
  };

  const mockSelector = (jobs?: JobResponse[]) => {
    jest
      .spyOn(StoreHooks, "useAppSelector")
      // @ts-ignore
      .mockImplementation((state) =>
        state({
          auth: { ...authInitialState },
          user: { ...userInitialState },
          job: { loading: false, jobs: jobs ? jobs : [] },
        }),
      );
  };

  beforeEach(() => {
    mockSelector();
  });

  it("shouldn't dispatch if job selector state 'jobs' have values stored", () => {
    const dispatch = mockDispatch();
    mockSelector([{ ...mock_jobs }]);
    renderCustomHook();
    expect(dispatch).toHaveBeenCalledTimes(0);
  });

  it("should dispatch fetchJobs when jobs array is equal to zero", () => {
    const dispatch = mockDispatch();
    renderCustomHook();
    expect(dispatch).toHaveBeenCalledTimes(1);
  });
});

import * as UseJobState from "@core/components";
import { Jobs } from "@core/components";
import { TableCellType, TableJobHeaders, jobLabels } from "@core/infrastructure";
import { render } from "@testing-library/react";
import React from "react";

jest.mock("./state/useJob", () => ({
  useJob: jest.fn(),
}));

describe("Jobs Testing", () => {
  const mockJobState = () =>
    jest.spyOn(UseJobState, "useJob").mockReturnValue({
      jobs: [],
      headers: [{ name: "name", type: TableCellType.NORMAL }],
      mapHeaders: [
        {
          keys: [{ key: TableJobHeaders.NAME }],
        },
      ],
    });

  const renderComponent = () => {
    return render(<Jobs />);
  };

  it("should render the User component", () => {
    mockJobState();
    const { getByText } = renderComponent();
    expect(getByText(jobLabels.title)).toContainHTML(jobLabels.title);
  });
});

import { useAdminGuardState } from "@components/admin";
import React, { FC, ReactElement } from "react";
import { Navigate } from "react-router-dom";

export interface AdminGuardProps {
  children: ReactElement;
}

export const AdminGuard: FC<AdminGuardProps> = ({ children }) => {
  const { token } = useAdminGuardState();

  if (token === "null") {
    return <Navigate to="/auth" replace />;
  }

  return children;
};

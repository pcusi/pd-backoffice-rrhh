import { CustomHeaders } from "@infrastructure/enum";

export interface UseAdminGuardStateProps {
  token: string;
}

export const useAdminGuardState = (): UseAdminGuardStateProps => {
  const token: string = `${localStorage.getItem(CustomHeaders.TOKEN)}`;

  return {
    token,
  };
};

import { Layouts } from "@infrastructure/enum";
import { StylesProps } from "@shared/styles.props";

export const adminStyles: StylesProps = {
  container: {
    marginLeft: Layouts.SIDEBAR,
  },
  wrapper: {
    mt: 7,
    p: 4,
    height: "100vh",
  },
};

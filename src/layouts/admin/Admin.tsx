import {
  AdminGuard,
  Navbar,
  Sidebar,
  adminStyles,
  useAdmin,
} from "@components/admin";
import { Box, Stack } from "@mui/material";
import React from "react";
import { Outlet } from "react-router";

export const Admin = () => {
  const {
    user,
    anchorEl,
    open,
    popoverClose,
    popoverClick,
    logout,
    notificationsLength,
  } = useAdmin();

  return (
    <Box>
      <Sidebar />
      <Box sx={adminStyles.container}>
        <Navbar
          user={user}
          anchorEl={anchorEl}
          popoverClick={popoverClick}
          popoverClose={popoverClose}
          open={open}
          logout={logout}
          notificationsLength={notificationsLength}
        />
        <Stack sx={adminStyles.wrapper}>
          <AdminGuard>
            <Outlet />
          </AdminGuard>
        </Stack>
      </Box>
    </Box>
  );
};

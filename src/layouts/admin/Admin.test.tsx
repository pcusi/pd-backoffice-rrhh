import * as UseAdminState from "@components/index";
import { Admin } from "@core/layouts";
import { authInitialState } from "@store/reducers/auth/auth.slice";
import { render } from "@testing-library/react";
import React from "react";
import { MemoryRouter, Route, Routes } from "react-router";

jest.mock("../../components/admin/state/useAdmin", () => ({
  useAdmin: jest.fn(),
}));

describe("Admin Test Layout", () => {
  const mockUseAdminState = () =>
    jest.spyOn(UseAdminState, "useAdmin").mockReturnValue({
      user: {
        ...authInitialState.user,
      },
      anchorEl: null,
      open: false,
      popoverClose: jest.fn(),
      popoverClick: jest.fn(),
      logout: jest.fn(),
      notificationsLength: 0,
    });

  const renderComponent = () => {
    render(
      <MemoryRouter initialEntries={["/admin"]}>
        <Routes>
          <Route path="/admin" element={<Admin />} />
        </Routes>
      </MemoryRouter>,
    );
  };

  it("should render the admin layout", () => {
    mockUseAdminState();
    renderComponent();
  });
});

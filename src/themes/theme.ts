import { createTheme } from "@mui/material";
import { ThemeOptions } from "@mui/system";

export const Palette = {
  primary: {
    main: "#4FD1C5",
    dark: "#006a63",
    light: "#ffffff",
    container: "#72f7ea",
    onContainer: "#00201d",
    glass: "rgba(255, 255, 255, .7)",
  },
  secondary: {
    main: "#4a6360",
    dark: "#33303C",
    light: "#ffffff",
    container: "#cce8e4",
    onContainer: "#051f1d",
  },
  tertiary: {
    main: "#7993af",
  },
  background: {
    primary: "#fafdfb",
    dark: "#191c1c",
    surface: "#fafdfb",
    onSurface: "#191c1c",
  },
  error: {
    main: "#ba1a1a",
    container: "#ffdad6",
    onContainer: "#410002",
  },
  text: {
    primary: "#4FD1C5",
    grey: "#dae5e2",
    border: "#6f7977",
    white: "#FFF",
    dark: "#3f4947",
    glass: "rgba(255, 255, 255, .7)",
  },
  fontSize: {
    h1: "32px",
    h2: "28px",
    h3: "24px",
    h4: "20px",
    h5: "18px",
    h6: "16px",
    body1: "14px",
    body2: "12px",
    overline: "14px",
    caption: "10px",
  },
};

export const themeOptions: ThemeOptions = {
  palette: {
    primary: {
      main: Palette.primary.main,
      dark: Palette.primary.dark,
      light: Palette.primary.light,
      container: Palette.primary.container,
      onContainer: Palette.primary.onContainer,
    },
    tertiary: {
      main: Palette.tertiary.main,
    },
    background: {
      primary: Palette.background.primary,
      dark: Palette.background.dark,
      surface: Palette.background.surface,
      onSurface: Palette.background.onSurface,
    },
    secondary: {
      main: Palette.secondary.main,
      dark: Palette.secondary.dark,
      light: Palette.secondary.light,
    },
    text: {
      primary: Palette.text.primary,
      secondary: Palette.text.grey,
      white: Palette.text.white,
      dark: Palette.text.dark,
    },
  },
};

export const theme = createTheme({
  palette: { mode: "light", ...themeOptions.palette },
  components: {
    MuiInputLabel: {
      styleOverrides: {
        root: {
          fontSize: Palette.fontSize.h6,
          color: Palette.text.grey,
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          border: `1px solid ${Palette.text.grey}`,
          color: Palette.text.dark,
          borderRadius: 16,
          "&:placeholder": {
            color: "black",
          },
          "&:hover": {
            border: `1px solid ${Palette.text.border}`,
          },
          "&.Mui-error": {
            border: `1px solid ${Palette.error.main}`,
            color: Palette.error.main,
          },
          "&.Mui-focused": {
            border: `1px solid ${Palette.secondary.main}`,
          },
        },
        notchedOutline: {
          border: `none`,
        },
      },
    },
    MuiButton: {
      defaultProps: {
        disableRipple: true,
        disableElevation: true,
      },
      styleOverrides: {
        contained: {
          backgroundColor: Palette.primary.dark,
          border: "none",
          color: Palette.primary.light,
          fontSize: Palette.fontSize.body1,
          fontWeight: 400,
          height: "36px",
          borderRadius: 16,
          textTransform: "none",
          "&:disabled": {
            backgroundColor: Palette.text.grey,
            color: Palette.secondary.dark,
            cursor: "not-allowed",
          },
          "&:hover": {
            backgroundColor: Palette.primary.dark,
          },
        },
      },
    },
  },
  typography: {
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
    h1: {
      fontSize: Palette.fontSize.h1,
    },
    h2: {
      fontSize: Palette.fontSize.h2,
    },
    h3: {
      fontSize: Palette.fontSize.h3,
    },
    h4: {
      fontSize: Palette.fontSize.h4,
    },
    h5: {
      fontSize: Palette.fontSize.h5,
    },
    h6: {
      fontSize: Palette.fontSize.h6,
    },
    body1: {
      fontSize: Palette.fontSize.body1,
    },
    body2: {
      fontSize: Palette.fontSize.body2,
    },
    caption: {
      fontSize: Palette.fontSize.caption,
    },
  },
});

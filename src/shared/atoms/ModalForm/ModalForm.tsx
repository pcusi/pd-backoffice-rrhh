import { LoadingButton } from "@mui/lab";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Stack,
} from "@mui/material";
import { ReactNode } from "react";
import { MdClose } from "react-icons/md";

export interface ModalFormProps {
  open: boolean;
  children: ReactNode;
  isValid: boolean | undefined;
  onSubmit: () => void;
  closeDialog: () => void;
  loading: boolean;
  dialogTitle: string;
  buttonTitle: string;
}

export const ModalForm = ({
  open,
  closeDialog,
  isValid,
  onSubmit,
  loading,
  dialogTitle,
  buttonTitle,
  children,
}: ModalFormProps) => {
  return (
    <form>
      <Dialog open={open} onClose={closeDialog}>
        <DialogTitle variant="h3" color="text.dark" fontWeight="bold">
          <Stack direction="row" justifyContent="space-between">
            {dialogTitle}
            <IconButton onClick={closeDialog} size="small">
              <MdClose />
            </IconButton>
          </Stack>
        </DialogTitle>
        <DialogContent>{children}</DialogContent>
        <DialogActions>
          <LoadingButton
            endIcon={<></>}
            loadingPosition="end"
            disabled={!isValid}
            variant="contained"
            onClick={onSubmit}
            type="button"
            loading={loading}
          >
            {buttonTitle}
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </form>
  );
};

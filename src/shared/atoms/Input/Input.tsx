import { InputController } from "@core/infrastructure";
import { Grid, TextField, Typography } from "@mui/material";
import { FC } from "react";
import { Controller } from "react-hook-form";

export interface InputProps extends InputController {}

export const Input: FC<InputProps> = ({
  ariaLabel,
  control,
  name,
  errorMessage,
  placeholder,
  errors,
  label,
  type,
  isComboBox,
  renderOptions,
}) => {
  return (
    <Grid container spacing={1}>
      <Grid item xs={12}>
        <Typography variant="body1" color="text.dark">
          {label}
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Controller
          name={name}
          control={control}
          rules={{ required: true }}
          render={({ field }) => {
            if (isComboBox) {
              return (
                <TextField
                  {...field}
                  select={true}
                  id={name}
                  fullWidth
                  type={type}
                  error={Boolean(errors[name])}
                  helperText={errors[name] && errorMessage}
                  placeholder={placeholder}
                  variant="outlined"
                >
                  {renderOptions}
                </TextField>
              );
            }

            return (
              <TextField
                {...field}
                id={name}
                aria-labelledby={ariaLabel}
                fullWidth
                type={type}
                error={Boolean(errors[name])}
                helperText={errors[name] && errorMessage}
                placeholder={placeholder}
                variant="outlined"
              />
            );
          }}
        />
      </Grid>
    </Grid>
  );
};

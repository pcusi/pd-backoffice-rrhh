import { ICustomTable } from "@core/infrastructure";
import { CustomCell, customCellStyles } from "@core/shared";
import {
  Button,
  Card,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import React, { FC } from "react";
import { MdAddCircle } from "react-icons/md";

export interface CustomTableProps extends ICustomTable {
  title: string;
  buttonTitle: string;
  onAddCell?: () => void;
}

export const CustomTable: FC<CustomTableProps> = ({
  buttonTitle,
  headers,
  data,
  mapHeaders,
  onEditCell,
  title,
  onAddCell,
  onDeleteCell,
}) => {
  return (
    <Card sx={customCellStyles.card} elevation={0}>
      <Stack
        direction="row"
        justifyContent="space-between"
        sx={customCellStyles.cardHeader}
      >
        <Typography variant="h2" color="text.dark" fontWeight={500}>
          {title}
        </Typography>
        <Button variant="contained" startIcon={<MdAddCircle />} onClick={onAddCell}>
          {buttonTitle}
        </Button>
      </Stack>
      <TableContainer>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              {headers.map(({ name }) => (
                <TableCell key={Math.random()} sx={customCellStyles.header}>
                  <Typography variant="body2" sx={customCellStyles.headerCell}>
                    {name}
                  </Typography>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row, _) => (
              <TableRow
                key={Math.random()}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                {headers.map(({ type }, index) => (
                  <CustomCell
                    json={[{ data: row, keys: mapHeaders[index].keys }]}
                    data={row}
                    key={Math.random()}
                    type={type}
                    onEditCell={onEditCell}
                    onDeleteCell={onDeleteCell}
                  />
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Card>
  );
};

import { StylesProps } from "@shared/styles.props";
import { Palette } from "@themes/theme";

export const customCellStyles: StylesProps = {
  card: {
    borderRadius: 8,
    border: "none",
  },
  cardHeader: {
    p: 2,
  },
  headerCell: {
    color: Palette.text.dark,
    fontWeight: 700,
    textTransform: "uppercase",
  },
};

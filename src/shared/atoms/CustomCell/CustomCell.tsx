import { JsonKey, TableHeaders, rolesCatalog } from "@core/infrastructure";
import { TableCellType } from "@infrastructure/enum";
import { Avatar, Button, Chip, Stack, TableCell, Typography } from "@mui/material";
import React, { FC } from "react";

export interface CustomCellProps extends CustomCellActions {
  json: CustomCellJsonData[];
  data: any;
  type: string;
}

export interface CustomCellJsonData {
  data: any;
  keys: JsonKey[];
}

export interface CustomCellActions {
  onEditCell?: (data: any) => void;
  onDeleteCell?: (data: any) => void;
}

export const renderChip = (
  key: JsonKey[] | undefined,
  value: string,
): JSX.Element => {
  switch (key![0].key) {
    case TableHeaders.ROLE:
      return (
        <Chip label={rolesCatalog[value]} color="secondary" variant="outlined" />
      );
    default:
      return <Chip label={value} color="secondary" variant="outlined" />;
  }
};

export const renderCell = (
  type: string,
  values: string[],
  props?: CustomCellActions,
  data?: any,
  key?: JsonKey[] | undefined,
): JSX.Element => {
  switch (type) {
    case TableCellType.CHIP:
      return (
        <TableCell component="th" scope="row">
          {renderChip(key, values[0])}
        </TableCell>
      );
    case TableCellType.OPTION:
      return (
        <TableCell component="th" scope="row">
          <Button onClick={() => props?.onEditCell!(data)}>{"Edit"}</Button>
          <Button onClick={() => props?.onDeleteCell!(data)}>{"Delete"}</Button>
        </TableCell>
      );
    case TableCellType.THUMBNAIL:
      return (
        <TableCell component="th" scope="row">
          <Stack direction="row" spacing={1}>
            <Avatar src="" variant="rounded" />
            <Stack direction="column" spacing={0.5}>
              <Typography variant="body1" color="text.dark">
                {values[0]}
              </Typography>
              <Typography variant="body2" color="text.dark">
                {values[1]}
              </Typography>
            </Stack>
          </Stack>
        </TableCell>
      );
    default:
      return (
        <TableCell component="th" scope="row">
          <Typography variant="body2" color="text.dark">
            {values[0]}
          </Typography>
        </TableCell>
      );
  }
};

export const CustomCell: FC<CustomCellProps & CustomCellActions> = ({
  onDeleteCell,
  onEditCell,
  data,
  json,
  type,
}) => {
  const values = json.map((v: CustomCellJsonData) =>
    v.keys.map((k) => v.data[k.key]),
  );
  const key = json.map((v: CustomCellJsonData) => v.keys);

  return renderCell(
    type,
    values[0],
    {
      onEditCell: onEditCell ? onEditCell : () => {},
      onDeleteCell: onDeleteCell ? onDeleteCell : () => {},
    },
    data,
    key[0],
  );
};

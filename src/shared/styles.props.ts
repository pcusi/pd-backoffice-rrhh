import { SxProps, Theme } from "@mui/system";

export interface StylesProps {
  [k: string]: SxProps<Theme>;
}

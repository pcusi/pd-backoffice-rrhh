import { CustomHeaders } from "@infrastructure/enum";
import axios, { AxiosResponse } from "axios";

const instance = axios.create({
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json; charset=utf-8",
    Authorization: `${localStorage.getItem(CustomHeaders.TOKEN)}`,
  },
});

instance.interceptors.response.use(
  (res: AxiosResponse) => {
    return res;
  },
  (err) => {
    if (err.request) {
      return Promise.reject(err.request);
    }

    return Promise.reject(err.message);
  },
);

export default instance;

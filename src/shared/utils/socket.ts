import { environment } from "@environment/environment";
import { CustomHeaders } from "@infrastructure/enum";
import { Socket, io } from "socket.io-client";

let socket: Socket;

export const initSocket = () => {
  socket = io(environment.wsUrl, {
    extraHeaders: {
      authorization: `${localStorage.getItem(CustomHeaders.TOKEN)}`,
    },
  });
};

export const getSocket = (): Socket => {
  return socket;
};

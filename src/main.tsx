import { globalStyles } from "@core/global.styles";
import "@core/index.css";
import { GlobalStyles, ThemeProvider } from "@mui/material";
import { router } from "@routes/backoffice.routes";
import { store } from "@store/store";
import { theme } from "@themes/theme";
import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { RouterProvider } from "react-router-dom";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <Provider store={store}>
    <GlobalStyles styles={globalStyles} />
    <ThemeProvider theme={theme}>
      <RouterProvider router={router} />
    </ThemeProvider>
  </Provider>,
);
